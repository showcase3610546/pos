"use client";

import { useSession, signIn, signOut } from "next-auth/react"
import { useRouter } from 'next/navigation'
import React, { useState, FormEvent, Suspense } from "react";
import { useSearchParams } from 'next/navigation'

interface FormData {
    email: string;
    password: string;
}

export default function SingIn() {

    const router = useRouter();
    const searchParams = useSearchParams()

    const [formData, setFormData] = useState<FormData>({
        email: '',
        password: '',
    });

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
        console.log(formData);
    };

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault(); // Prevents the default form submission behavior

        const callbackUrl = searchParams.get('callbackUrl')
        router.refresh();

        signIn('credentials', { redirect: false, email: formData.email, password: formData.password, callbackUrl: callbackUrl || '/dashboard' }).then((res) => {
            if (res?.error === null) {
                if (res.url) {
                    // dispatch(successMessage("Successfully Login"));
                    router.push(res.url)
                }
            } else {
                console.log(res?.error)
            }
        });

    };

    return (
        <Suspense>
            <div className="flex justify-center">
                <div className=" mt-24 w-2/6 bg-gray-700 text-white p-6 rounded-md">
                    <div className="text-white text-center text-xl mb-4">Sign In</div>
                    <form onSubmit={handleSubmit}>

                        <div className="flex flex-col gap-2 ml-20">
                            <div>Email:</div>
                            <div>
                                <input
                                    onChange={handleInputChange}
                                    name="email"
                                    value={formData.email}
                                    type="email" className="form-input rounded-lg text-gray-600" />
                            </div>
                        </div>
                        <div className="flex flex-col gap-2 ml-20 mt-10">
                            <div>Password:</div>
                            <div>
                                <input
                                    onChange={handleInputChange}
                                    name="password"
                                    value={formData.password}
                                    type="password" className="form-input rounded-lg text-gray-600" />
                            </div>
                        </div>
                        <div className="flex justify-center mt-6">
                            <button type="submit" className="bg-white px-4 py-2 text-gray-700 rounded-lg">
                                Login
                            </button>
                        </div>
                    </form>

                </div>

            </div>
        </Suspense>
    )


}