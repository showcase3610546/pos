"use client";

import Cart from "@/components/cart"
import Link from 'next/link'
import { useSession, signIn, signOut } from "next-auth/react"

export default function DashboardLayout({
    children, // will be a page or nested layout
}: {
    children: React.ReactNode
}) {
    return (
        <section className="bg-slate-200">
            {/* <div>Hello</div> */}
            <div className="flex h-screen">
                {/* Include shared UI here e.g. a header or sidebar */}
                <nav className="w-20 border-r-2 border-blue-300 flex flex-col space-y-14 px-4 py-8">
                    <Link href="/dashboard">
                        <svg className="fill-blue-800 h-8" viewBox="0 0 28 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14 3.79765L21 10.1506V21.1765H18.2V12.7059H9.8V21.1765H7V10.1506L14 3.79765ZM14 0L0 12.7059H4.2V24H12.6V15.5294H15.4V24H23.8V12.7059H28L14 0Z" />
                        </svg>
                    </Link>
                    <Link href="/dashboard/sale">
                        <svg className="fill-blue-800 h-8" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13 0C5.832 0 0 5.832 0 13C0 20.168 5.832 26 13 26C20.168 26 26 20.168 26 13C26 5.832 20.168 0 13 0ZM13 2C19.065 2 24 6.935 24 13C24 19.065 19.065 24 13 24C6.935 24 2 19.065 2 13C2 6.935 6.935 2 13 2ZM10 8V19H12V15H14.5C16.431 15 18 13.431 18 11.5C18 9.569 16.431 8 14.5 8H10ZM12 10H14.5C15.327 10 16 10.673 16 11.5C16 12.327 15.327 13 14.5 13H12V10Z" />
                        </svg>

                    </Link>
                    <Link href="/dashboard/orders">
                        <svg height="35" viewBox="0 0 21 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.42857 12.2448H14.5714M6.42857 16.8448H14.5714M6.42857 7.64478H14.5714M1 3.04478C1 2.73978 1.14298 2.44727 1.3975 2.2316C1.65201 2.01594 1.99721 1.89478 2.35714 1.89478H18.6429C19.0028 1.89478 19.348 2.01594 19.6025 2.2316C19.857 2.44727 20 2.73978 20 3.04478V24.8948L15.25 22.0198L10.5 24.8948L5.75 22.0198L1 24.8948V3.04478Z" stroke="rgb(30 64 175)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </Link>
                    <div className="cursor-pointer" onClick={() => signOut()}>
                        <svg
                            className="stroke-blue-800"
                            height="35"
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M9 21H5C3.89543 21 3 20.1046 3 19V5C3 3.89543 3.89543 3 5 3H9"
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            />
                            <path
                                d="M16 17L21 12L16 7"
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            />
                            <path
                                d="M21 12H9"
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            />
                        </svg>
                    </div>
                </nav>
                <div className="flex w-full">
                    <div className="w-4/6">
                        {children}
                    </div>
                    <div className="w-2/6">
                        <Cart />
                    </div>
                </div>


            </div>

        </section>
    )
}