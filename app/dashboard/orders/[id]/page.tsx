"use client";

import React, { useState, FormEvent, useEffect } from "react";
import { useSession } from "next-auth/react"
import Image from 'next/image';
import { useParams } from 'next/navigation'

export default function OrderDetail() {
    const params = useParams<{ id: string; }>()

    interface Order {
        id: number,
        orderNo: string,
        totalPrice: number,
        subTotalPrice: number,
        itemCount: number,
        user: any,
        orderItems: any
    }

    const { data: session } = useSession()
    const [order, setOrder] = useState<Order>();
    useEffect(() => {
        //set orders
        fetch(process.env.NEXT_PUBLIC_API_URL + '/api/orders/' + params.id, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {
                setOrder(data.order)
            });
    }, [session])

    const imageLoader = ({ src, width, quality }: { src: string; width: number; quality?: number }) => {
        return `${src}?w=${width}&q=${quality || 75}`
    }

    return <>
        <div className="p-4">
            <div className="text-lg font-semibold">
                <p>Order No:  #{order?.orderNo} </p>
            </div>
            <div className="pt-4">
                {/* Items */}
                {
                    order?.orderItems.map((orderItem: any, index: number) => (
                        <div key={index} className="bg-blue-200 w-2/3 p-2 mt-2">
                            <div className="flex justify-between">
                                <div className="flex space-x-4">
                                    <div>
                                        <Image loader={() => imageLoader({ src: orderItem.imageUrl, width: 200, quality: 75 })}
                                            src="https://cloud-atg.moph.go.th/quality/sites/default/files/default_images/default.png"
                                            width={100}
                                            height={100}
                                            alt="item" />
                                    </div>
                                    <div>
                                        <div className="font-semibold">{orderItem.name}</div>
                                        <div>Qty: {orderItem.qty}</div>
                                    </div>
                                </div>
                                <div className="flex space-x-4">
                                    <div >
                                        <div>Per Price</div>
                                        <div className="font-semibold">Total</div>
                                    </div>
                                    <div>
                                        <div className="flex justify-end">: ${orderItem?.subPrice}</div>
                                        <div className="font-semibold flex justify-end">: ${orderItem?.totalPrice}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }


                {/* End Items */}
            </div>

            <div className="font-semibold text-xl mt-4">
                <div>Sale By: {order?.user?.name}</div>
                <div>Sub Total: ${order?.subTotalPrice}</div>
                <div>Total: ${order?.totalPrice}</div>
            </div>
        </div>
    </>
}