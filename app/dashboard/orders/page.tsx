"use client";

import React, { useState, FormEvent, useEffect } from "react";
import { useSession } from "next-auth/react"
import Link from 'next/link'

export default function Orders() {
    const { data: session } = useSession()
    interface Order {
        id: number,
        orderNo: string,
        totalPrice: number,
        subTotalPrice: number,
        createdAt: string,
        itemCount: number,
        user: any
    }
    const [orders, setOrders] = useState<Order[]>([]);
    const [page, setPage] = useState(1);
    const [pagination, setPagination] = useState<any>();


    useEffect(() => {
        //set orders
        fetch(process.env.NEXT_PUBLIC_API_URL + `/api/orders?page=${page}&limit=10 `, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {
                setOrders(data.orders.data)
                setPagination(data.orders.meta);
            });
    }, [session, page])



    return <>
        <div>
            <div className="p-4 text-xl font-bold">Sale Orders</div>
            <div className="p-4">
                <table className="table-auto border-separate border-2 border-slate-500 w-full">
                    <thead>
                        <tr className="bg-blue-200 h-10">
                            <th className="border-2 border-slate-600">Order No</th>
                            <th className="border-2 border-slate-600">Total Item</th>
                            <th className="border-2 border-slate-600">Total Amount</th>
                            <th className="border-2 border-slate-600">Sale User</th>
                            <th className="border-2 border-slate-600">Created At</th>
                            <th className="border-2 border-slate-600">Detail</th>
                        </tr>
                    </thead>
                    <tbody className="text-center">
                        {
                            orders.map((order, index) => {
                                const [date, timeZone] = order.createdAt.split('T');
                                const [timePoint] = timeZone.split('+');
                                const time = timePoint.split('.')[0];

                                return (
                                    <tr key={index} className="bg-blue-100 h-10">
                                        <td className="border-2 border-slate-400">{order.orderNo}</td>
                                        <td className="border-2 border-slate-400">{order.itemCount}</td>
                                        <td className="border-2 border-slate-400">{order.totalPrice}</td>
                                        <td className="border-2 border-slate-400">{order.user?.name}</td>
                                        <td className="border-2 border-slate-400">{date} {time}</td>
                                        <td className="border-2 border-slate-400"><Link href={`/dashboard/orders/${order.id}`}>view</Link></td>
                                    </tr>
                                );
                            })

                        }

                    </tbody>

                </table>
                <div className="flex justify-end mt-4">
                    <nav aria-label="Page navigation">
                        <ul className="inline-flex items-center -space-x-px">
                            {
                                page > 1 &&
                                <li>
                                    <a onClick={() => setPage(page - 1)} className="px-3 py-2 ml-0 cursor-pointer leading-tight text-gray-500 bg-white border border-slate-500 rounded-l-lg hover:bg-blue-500 hover:text-white">Previous</a>
                                </li>
                            }

                            {
                                page < pagination?.lastPage &&
                                <li>
                                    <a onClick={() => setPage(page + 1)} className="px-3 py-2 cursor-pointer leading-tight text-gray-500 bg-white border border-slate-500 rounded-r-lg hover:bg-blue-500 hover:text-white">Next</a>
                                </li>
                            }
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </>
}