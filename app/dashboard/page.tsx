import Link from "next/link"

export default function Dashboard() {

    return <>
        <div className="p-4">
            <h2 className="text-xl font-semibold mb-4">Welcome to POS</h2>
            <div className="flex space-x-4">
                <Link href={`/dashboard/sale`}>
                    <div className="w-40 h-20 bg-blue-200 hover:bg-blue-300 grid place-content-center cursor-pointer rounded-lg">
                        Sale
                    </div>
                </Link>
                <Link href={`/dashboard/orders`}>
                    <div className="w-40 h-20 bg-blue-200 hover:bg-blue-300 grid place-content-center cursor-pointer rounded-lg">
                        Order History
                    </div>
                </Link>
            </div>

        </div>
    </>
}