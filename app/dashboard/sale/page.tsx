"use client";

import { clearCart, addCart } from "@/lib/features/cart-slice"
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, useAppSelector } from "@/lib/store";
import React, { useState, FormEvent, useEffect } from "react";
import { useSession } from "next-auth/react"
import Image from 'next/image';

export default function Sale() {
    const { data: session } = useSession()
    const [src, setSrc] = React.useState('https://cloud-atg.moph.go.th/quality/sites/default/files/default_images/default.png');
    interface Category {
        id: number,
        name: string
    }

    interface Product {
        productId: number,
        name: string,
        subPrice: number,
        imgUrl: string,
        categoryId: number,
        qty: number
    }

    const dispatch = useDispatch<AppDispatch>();

    const [categories, setCategories] = useState<Category[]>([]);
    const [products, setProducts] = useState<Product[]>([]);
    const [selectedCategory, setSelectedCategory] = useState<Category>();



    useEffect(() => {
        if (session) {
            //set categories
            fetch(process.env.NEXT_PUBLIC_API_URL + '/api/categories', {
                method: 'GET',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${session?.user?.token}`
                }
            })
                .then((res) => res.json())
                .then((data) => {
                    setCategories(data.categories.data)
                    setSelectedCategory(data.categories.data[0]);
                });

            //set products
            fetch(process.env.NEXT_PUBLIC_API_URL + '/api/products', {
                method: 'GET',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${session?.user?.token}`
                }
            })
                .then((res) => res.json())
                .then((data) => {
                    const productData = data.products?.data.map((product: any) => ({
                        productId: product.id,
                        name: product.name,
                        subPrice: product.costPrice,
                        imgUrl: product.imageUrl,
                        categoryId: product.categoryId,
                        qty: product.initialQty
                    }));
                    setProducts(productData)
                });
        }

    }, [session])




    const addToCart = (product: any) => {

        dispatch(addCart(product));
    }

    const handleCategorySelect = (category: Category) => {
        setSelectedCategory(category);
    }

    const handleImgLoadingError = (e: any) => {

        // default image add, if associated image is not available...
        e.target.src = 'https://cloud-atg.moph.go.th/quality/sites/default/files/default_images/default.png';

    };

    const imageLoader = ({ src, width, quality }: { src: string; width: number; quality?: number }) => {
        return `${src}?w=${width}&q=${quality || 75}`
    }

    return <>
        <div className="p-4">
            <div className="text-lg">Category</div>
            <div className="grid grid-cols-2 xl:grid-cols-8 sm:grid-cols-4 gap-4 mt-4 text-gray-800">
                {categories.map((category, index) => (
                    <div key={index} onClick={() => handleCategorySelect(category)} className={`px-4 py-2 border-2 border-blue-800 rounded-md cursor-pointer ${selectedCategory?.id === category.id ? "bg-blue-600 text-white" : ""} `}>
                        {category.name}
                    </div>
                ))}
            </div>
            <div className="text-lg mt-4">Product</div>
            <div className="grid grid-cols-5 gap-4 mt-4">
                {products?.map((product, index) => (

                    selectedCategory?.id === product.categoryId &&
                    <div key={index} style={{ height: "400px" }} className="border-2 border-blue-400 py-2 rounded-md text-gray-600 relative">
                        <div className="">
                            <div className="h-56">
                                <Image
                                    loader={() => imageLoader({ src: product.imgUrl ?? src, width: 200, quality: 75 })}
                                    src={src}
                                    width={200}
                                    height={200}
                                    alt={product.name}
                                />
                            </div>

                            <div className="text-md text-center">{product.name.length > 20 ? product.name.slice(0, 20) + '...' : product.name}</div>
                            <div className="text-xl font-bold text-center mt-2">{product.subPrice} $</div>
                        </div>
                        <div className="absolute bottom-5 flex justify-center w-full">
                            <div onClick={() => addToCart(product)} className="p-4 bg-blue-600 rounded-sm cursor-pointer text-center text-white">Add to cart</div>
                        </div>
                    </div>


                ))}
            </div>

        </div>
    </>
}