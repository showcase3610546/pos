"use client";

import { clearCart, addCart, increaseItem, decreaseItem } from "@/lib/features/cart-slice"
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, useAppSelector } from "@/lib/store";
import { useRouter } from 'next/navigation'
import { useSession } from "next-auth/react"
import React, { useState, FormEvent, useEffect } from "react";
import { clear } from "console";
import Image from 'next/image';

const Cart = () => {

    const { data: session } = useSession()
    const dispatch = useDispatch<AppDispatch>();

    const cart = useAppSelector((state) => state.cartReducer.value)
    const router = useRouter();

    const handleOrder = () => {
        return fetch(process.env.NEXT_PUBLIC_API_URL + '/api/orders', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            },
            body: JSON.stringify({
                totalPrice: cart.totalPrice,
                subTotalPrice: cart.subTotalPrice,
                userId: session?.user.user.id,
                orderItems: cart.orderItems.map((orderItem) => {
                    return {
                        productId: orderItem.productId,
                        name: orderItem.name,
                        imageUrl: orderItem.imgUrl,
                        qty: orderItem.qty,
                        subPrice: orderItem.subPrice,
                        totalPrice: orderItem.totalPrice
                    }
                })
            })
        })
            .then((res) => res.json())
            .then((data) => {
                dispatch(clearCart());
                router.push('/dashboard/orders')
            });
    }

    const imageLoader = ({ src, width, quality }: { src: string; width: number; quality?: number }) => {
        return `${src}?w=${width}&q=${quality || 75}`
    }

    return (
        <div>
            <div className="p-4 bg-blue-200 text-white" style={{ height: "600px", overflow: "scroll" }}>
                {
                    cart.orderItems.map((item, index) => (
                        <div key={index} className="flex justify-between bg-blue-500 p-4 mt-2">
                            <div className="flex space-x-4">
                                <Image loader={() => imageLoader({ src: item.imgUrl ?? `https://cloud-atg.moph.go.th/quality/sites/default/files/default_images/default.png`, width: 200, quality: 75 })}
                                    src="https://cloud-atg.moph.go.th/quality/sites/default/files/default_images/default.png"
                                    width={100}
                                    height={100}
                                    alt={item.name} />
                                <div className="flex flex-col w-36">
                                    <div>{item.name}</div>
                                    <div>{item.subPrice} $</div>
                                </div>
                            </div>

                            <div>
                                <div className="flex space-x-4 p-4 bg-blue-400">
                                    <div onClick={() => dispatch(decreaseItem(item))} className="text-lg font-bold cursor-pointer">-</div>
                                    <div>{item.qty}</div>
                                    <div onClick={() => dispatch(increaseItem(item))} className="text-lg font-bold cursor-pointer">+</div>
                                </div>
                            </div>
                            <div>
                                $ {item.totalPrice}
                            </div>

                        </div>
                    ))
                }

            </div>
            <div className="bg-black text-white p-4 flex flex-col space-y-4">
                <div className="flex justify-between">
                    <p>Sub Total</p>
                    <p>${cart.subTotalPrice}</p>
                </div>
                <div className="flex justify-between">
                    <p>Total</p>
                    <p>$ {cart.totalPrice}</p>
                </div>
            </div>
            <div className="mt-2 flex justify-between">
                <div onClick={() => dispatch(clearCart())} className="bg-red-600 w-32 p-4 text-white rounded-lg text-center cursor-pointer">
                    Clear
                </div>
                {
                    cart.orderItems.length ?
                        <div onClick={() => handleOrder()} className="bg-blue-600 w-64 p-4 text-white rounded-lg text-center cursor-pointer">
                            Place Order
                        </div>
                        :
                        <div className="bg-blue-300 w-64 p-4 text-white rounded-lg text-center">
                            Place Order
                        </div>
                }

            </div>
        </div>

    )
}

export default Cart;