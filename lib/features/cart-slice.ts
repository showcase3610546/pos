import { createSlice, PayloadAction } from "@reduxjs/toolkit"

type CartItem = {
    productId: number,
    name: string,
    subPrice: number,
    totalPrice: number,
    qty: number,
    imgUrl: string
}

type CartState = {
    totalPrice: number,
    subTotalPrice: number,
    orderItems: CartItem[];
}

type InitialState = {
    value: CartState;
}

const initialState = {
    value: {
        totalPrice: 0,
        subTotalPrice: 0,
        orderItems: []
    } as CartState,
} as InitialState;

const calculateTotal = (state: any) => {
    state.value.subTotalPrice = state.value.orderItems.reduce((acc: any, item: any) => acc + item.totalPrice, 0);
    state.value.totalPrice = state.value.subTotalPrice; // Assuming total includes only subtotal, no tax/shipping
}

export const cart = createSlice({
    name: "auth",
    initialState,
    reducers: {
        clearCart: () => {
            return initialState
        },
        addCart: (state, action: PayloadAction<CartItem>) => {
            var newItem = action.payload;
            console.log('newItem: ', newItem)
            const existingItem = state.value.orderItems.find(item => item.productId === newItem.productId);

            if (existingItem) {
                existingItem.qty += 1;
                existingItem.totalPrice = existingItem.qty * existingItem.subPrice;
            } else {
                const newObj = { ...newItem, totalPrice: newItem.subPrice }
                state.value.orderItems.push(newObj);
            }
            calculateTotal(state);
        },
        increaseItem: (state, action: PayloadAction<CartItem>) => {
            const item = action.payload;
            const existingItem = state.value.orderItems.find(value => value.productId === item.productId);
            if (existingItem) {
                existingItem.qty += 1;
                existingItem.totalPrice = existingItem.qty * existingItem.subPrice;
            }
            calculateTotal(state);

        },

        decreaseItem: (state, action: PayloadAction<CartItem>) => {
            const item = action.payload;
            const existingItem = state.value.orderItems.find(value => value.productId === item.productId);
            if (existingItem) {
                if (existingItem.qty === 1) {
                    const updatedOrderItems = state.value.orderItems.filter(value => value.productId !== item.productId);

                    // Update the state with the new array
                    state.value.orderItems = updatedOrderItems;
                } else {
                    existingItem.qty -= 1;
                    existingItem.totalPrice = existingItem.qty * existingItem.subPrice;
                }

            }
            calculateTotal(state);
        },
    }
})

export const { clearCart, addCart, increaseItem, decreaseItem } = cart.actions;

export default cart.reducer;