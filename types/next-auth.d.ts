import NextAuth from "next-auth"

import { JWT, Session, User } from "next-auth/next"
declare module "next-auth" {
    interface User {
        user: {
            id: string,
            name: string,
            email: string
        };
        token: string
    }

    /**
     * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
     */
    interface Session {
        user: any;
        token: string;
    }
}